import { useState } from "react"
import TrackerInput from '../components/TrackerInput'
import TrackerTransactions from '../components/TrackerTransactions'
import moment from 'moment'


const TrackerScreen = () => {
    let [balance, setBalance ] = useState(0)
    let [transactions, setTransactions] = useState([])
    
    const getCurrTime = () => {
        return moment().format('YYYY-MM-DDTHH:mm:ss')
    }
    const doAction = (action, val) => {
        if(!val) return
        if(action === 'add') balance+=Number(val)
        if(action === 'remove') balance-=Number(val)
        setBalance(balance)
        transactions.push({
            value: val,
            action: action === 'add' ? 'Added': 'Removed',
            time: getCurrTime()
        })
        setTransactions(transactions)

    }
    return (
        <div>
            <h1 style={{textAlign: 'center'}}>Expense Tracker - Basic:</h1>
            <br />
            <TrackerInput balance={balance} doAction={doAction} />
            <TrackerTransactions transactions={transactions} />
        </div>
    )
}

export default TrackerScreen