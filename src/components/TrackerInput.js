import { useState } from "react"
import './TrackerInput.css'
const TrackerInput = (props) => {

    const [val, setVal] = useState('')
    const [hasError, setHasError] = useState('')

    const checkInputVal = (v) => {
        if(isNaN(Number(v))) setHasError('Please give only numbers')
        else {
            setHasError('')
            setVal(Number(v))
        }
    }

    const getErrorMessage = () => {
        if(hasError) return (<div className="error">{hasError}</div>)
    }

    return (
        <div className="ti">
            <div className="container">
                <div className="info"> Balance: {props.balance}</div>
                <input type="text" value={val} onInput={({target}) => checkInputVal(target.value)} />
                <div>
                    <button onClick={() => props.doAction('add', val)}>Add</button>
                    <button onClick={() => props.doAction('remove', val)}>Remove</button>
                </div>
                {getErrorMessage()}
            </div>
        </div>
    )
}

export default TrackerInput