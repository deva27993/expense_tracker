import './TrackerTransactions.css'
const TrackerTransactions = (props) => {
    return (
        <div className="tt">
            <div className="container">
                <b>Transactions:</b>
                <br />
                <br />
                {props.transactions.map(x => <div>{x.time} - {x.value} - {x.action}</div>)}
            </div>
        </div>
    )
}

export default TrackerTransactions